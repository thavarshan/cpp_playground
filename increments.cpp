#include <iostream>
#include <string>

using namespace std;

int main()
{
    int a = 5;

    printf("When a = 5 then,\n");

    ++a;
    printf("++a is %d\n", a);

    a = 5;

    a++;
    printf("a++ is %d\n", a);

    a = 5;

    --a;
    printf("--a is %d\n", a);

    a = 5;

    a--;
    printf("a-- is %d\n", a);
}
